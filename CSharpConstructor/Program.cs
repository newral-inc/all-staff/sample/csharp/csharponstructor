﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpConstructor
{
    class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public Person() : this("", 0) { }

        public Person(string name, int age) : base()
        {
            this.Name = name;
            this.Age = age;
        }

        public override string ToString()
        {
            return $"Name={Name} Age={Age}";
        }
    }

    class PersonEx : Person
    {
        public string Address { get; set; }

        public PersonEx() : this("", 0, "") { }

        public PersonEx(string name, int age, string address) : base(name, age)
        {
            this.Address = address;
        }

        public override string ToString()
        {
            var str = base.ToString();
            return $"{str} Address={Address}";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var person1 = new Person();
            Console.WriteLine(person1);

            var person2 = new Person("Taro", 20);
            Console.WriteLine(person2);

            var personEx1 = new PersonEx();
            Console.WriteLine(personEx1);

            var personEx2 = new PersonEx("Jiro", 19, "Tokyo");
            Console.WriteLine(personEx2);
        }
    }
}
